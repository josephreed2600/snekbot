debug = true;
last = null;
package = require('./package.json');

console.log('Starting ' + package.name + ' v' + package.version);

console.log('Requiring stuff...');
Discord = require('discord.io');
logger = require('winston');
var auth = require('./auth.json');
sudoers = require('./sudoers.json');
request = require('request');

require('./sleep');
console.log('...done.');

console.log('Creating bot...');
/*/var/**/ bot = new Discord.Client({
   token: auth.token,
   autorun: true
});
console.log('...done.');

console.log('Importing commands...');
require('./send');
require('./commands/normal');
require('./commands/privileged');
console.log('...done.');

bot.on('ready', function (evt) {
	console.log(package.name + ' ready');
	console.log('=============');
});

bot.on('message', function (user, userID, channelID, message, evt) {
	// ignore our own messages
	if(userID == auth.id) return;
	
	if(debug) last = evt;
	
	// If the line begins with '$' or '#', evaluate it as a command
	// otherwise, treat it as a message
	var line = message.split(' ');
	switch(line.shift()) {
		case '$': // do normal user stuff
			dolla(user, userID, channelID, line.join(' '), evt);
			break;
		case '#': // do privileged user stuff
			pound(user, userID, channelID, line.join(' '), evt);
			break;
		default: // treat as message
			interpret(user, userID, channelID, message, evt);
			break;
	}
	
});

/*function send(channelID,message) {
	bot.sendMessage({
		to: channelID,
		message: message
	});
}/**/

function dolla(user, userID, channelID, message, evt) {
	var output = null;
	var line = message.split(' ');
	
	if(!normal_commands.hasOwnProperty(line[0])) {
		if(privileged_commands.hasOwnProperty(line[0])) {
			output = line[0] + ': permission denied';
		} else {
			output = line[0] + ': command not found';
		}
	} else {
		output = normal_commands[line[0]](line, evt);
	}
	
	send(channelID, output);
}

function pound(user, userID, channelID, message, evt) {
	if(!evt.d.member.roles.includes(sudoers.role)) {
		output = 'Username is not in the sudoers file. This incident will be reported.';
		send(channelID, output);
		send(sudoers.reportChannelID,'sudo incident report:\n```json\n'+JSON.stringify(evt,null,2)+'```');
		return;
	}
	var output = null;
	var line = message.split(' ');
	if(!privileged_commands.hasOwnProperty(line[0])) {
		dolla(user, userID, channelID, message, evt);
		return;
	}
	output = privileged_commands[line[0]](line, evt);
	send(channelID, output);
}

function interpret(user, userID, channelID, message, evt) {
	var line = message.split(' ');
}
